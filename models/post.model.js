const mongoose = require('mongoose')

const postSchema = new mongoose.Schema({
    author: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
    },
    title: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true,
    },
    liked: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'User',
        },
    ],
    disliked: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'User',
        },
    ],
    comments: [
        {
            user: {
                type: mongoose.Types.ObjectId,
                ref: 'User',
            },
            content: String,
        },
    ],
})

const Post = mongoose.model('Post', postSchema)

module.exports = Post
