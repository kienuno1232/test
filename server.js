const express = require('express')
const helmet = require('helmet')
const config = require('./config/config')
const bodyParser = require('body-parser')

require('./helpers/connectDB')

const userRouter = require('./modules/user/user.route')
const generateUser = require('./config/initDB')
const { post } = require('./modules/user/user.route')

const app = express()
app.set('view engine', 'ejs')
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)
app.use(bodyParser.json())
app.use(helmet())

app.use('/v1/user', userRouter)

const handleError = (err, req, res, next) => {
    res.status(err.status || 500).json({
        status: err.status || 500,
        message: err.message,
    })
}

app.use(handleError)

app.listen(config.PORT)

app.set('view engine','ejs')
app.use(express.urlencoded({extended : true}));
app.use(express.static("config"));
app.use(express.static("views"));
app.get("/v1/user/login",(req, res) => {
    res.render("login",{message: null});
}); 
app.get("/v1/user/register",(req, res) => {
    res.render('register',{message: null});
}); 




