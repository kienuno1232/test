const joi= require('@hapi/joi')
//register
const registerValidation = data => {
    const schema=joi.object({
        username: joi.string()
            .min(3)
            .max(30)
            .required(),
        password: joi.string()
            .min(6)
            .max(30, 'utf8')
            .required(),
        email:  joi.string()
            .min(6)
            .max(40)
            .required()
            .email()
    })
    return schema.validate(data)
}
module.exports= {registerValidation}